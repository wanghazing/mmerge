# mmerge

`mmerge` 是一个命令行工具，它能将 madp 组件/页面的创建和更新工作自动化，减少人工操作带来的误差。

`mmerge` 在合并 schema.json 文件前，会将涉及改动的原始文件备份，防止合并失败导致原文件丢失，并且 `mmerge` 提供了回滚指令，支持将指定的合并操作撤销。
对于组件类型的新增，可以使用`mmerge ui`命令，在图形界面中完成操作。

## 使用方法

### 初始化工作空间

`mmerge`使用的默认 madp 配置为

```toml
madp_location = "./example/testapp" # madp文件夹地址
output = "." # 任务输出地址
```

如果需要修改配置，可以通过输入指令`mmerge init`在当前目录下生成一个 config.toml 文件

### 创建合并任务

输入命令建立一个合并任务

```
mmerge add [taskid]
```

`taskid` 可以是任何可作为文件夹名称的字符，建议包含日期等方便描述版本的标志，例如

```
mmerge add task-20230311
```

> 注意：此操作会被覆盖同名任务产生的资源

#### 可选参数

| 参数     | 说明         | 示例                                                    |
| -------- | ------------ | ------------------------------------------------------- |
| --config | 配置文件路径 | `mmerge add --config D:\\path\\to\\a\\task.config.toml` |

未指定配置文件的情况下，`mmerge` 将应用默认配置：

```toml
# 注意：不用的配置用空字符串或空数组,不要删掉

[app_config] #应用配置
app_id = "default" # 应用id

[pre_publish_config] #定时发版
time = "2023-08-03 22:00:00"
version = "1.1.4.589"
check_unit = 30000

[class_page] #页面类型(一般不会改)
pages = []
names = []

[class_component] #组件类型
components = []
names = []
sub_schema = []

# mmerge仅支持新增页面，页面实例的修改需要考虑不同环境的差异，请手动操作
[instance_page]
pages = []
names = []
types = []
sub_schema = []

[instance_component] #组件实例
components = []
names = []
types = []
sub_schema = []

[stage] #场景
# example:
stage_ids = []
```

你可以使用`mmerge print`指令打印当前使用的配置

程序执行完成后，会在当前目录生成一个名为`[taskid]`的文件夹，其中包含以下内容

```toml
├─taskid # 任务文件夹
  ├─backup
  │  └─components
  ├─class
  │  └─components
  ├─instance
  │  ├─components
  │  └─pages
  ├─stage
  └─task.config.toml
```

如果命令提供了`--config`参数,`task.config.toml`文件将会使用指定文件的内容

### 执行 merge 任务

将需要发布的页面类型文件、组件类型文件、组件实例文件分别放入`taskid/class/pages`、`taskid/class/components`、`taskid/instance/components`文件夹
如果需要合并场景配置，将你的`stage.json`文件放入`taskid/stage`文件夹

```
mmerge run [taskid]
```

`mmerge`将比对你提供的页面类型和组件类型文件，复制文件并修改`config.schema.json`文件,如果原配置已经注册了同名 schema.json 文件,将覆盖原页面/组件文件，并更新在`config.schema.json`中注册的页面/组件类型名称

对于组件实例，将复制并覆盖所有实例文件，在`_list.json`文件头部添加新组件，修改同 id 组件配置

`mmerge`会比对任务中的`stage.json`和目标的`stage.json`文件,在头部添加新场景，修改同 id 场景配置

> 对于场景,建议从生产环境下发一份场景配置表，各环境统一后再进行合并操作

### 任务回退

`mmerge`在合并过程中遇到异常，将回退所有涉及改动的文件

如果合并成功后发现配置异常,或其他原因需要回退到原来的配置，可以执行回退命令：

```
mmerge rollback [taskid]
```

此命令将此任务保存的备份文件复制回到 madp 配置目录,所以建议只回退最近的版本

## 定时发版

mmerge 支持定时发版，创建完成任务后，在 task.config.toml 文件的`pre_publish_config`配置项中，可以配置定时发版的时间，需要发布的版本号，编辑完成后，使用命令

```
mmerge publish [taskid]
```

执行定时发版任务

## 图形界面

命令`mmerge ui`可以运行一个本地服务器，打开浏览器输入`http://127.0.0.1:1644`可以通过图形界面完成组件类型新增操作

## 本地构建

参考[环境配置](https://www.rust-lang.org/zh-CN/learn/get-started)
