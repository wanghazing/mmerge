mod tests {

    #[test]
    pub fn find_str() {
        let base_string: String = String::from("abc.schema.json");
        assert_eq!(base_string.find("schema"), Some(4));
        match base_string.find("pages/") {
            Some(_) => println!("找到了？"),
            _ => print!("找不到"),
        }
    }
    #[test]
    pub fn print_win_path() {
        let test_str = "C:\\\\Users\\\\sb\\\\do";
        println!(r#"{}"#, test_str);
        println!("{}", test_str);
    }

    // #[test]
    // pub fn test_map() {
    //     assert_eq!(
    //         HashMap::<String, String>::new(),
    //         HashMap::<String, String>::new()
    //     )
    // }

    #[test]
    pub fn join_path() {
        use std::path;
        let pth: path::PathBuf = [r"C:\", "windows/systemWow64", "system32.dll"]
            .iter()
            .collect();
        // print!("{}", pth.as_os_str().to_str().unwrap());
        for i in 0..10 {
            println!(
                "{}",
                pth.join(format!("{}.txt", i)).as_os_str().to_str().unwrap()
            );
        }
    }

    #[test]
    /**
     * 解析madp配置文件
     */
    pub fn encode_madp_config() {
        use crate::config;

        const DEFAULT_MADP_CONFIG: &str = r#"
  madp_location = './example' # madp文件夹地址
  output = "." # 任务输出地址
      "#;

        let madp_config: config::MadpConfig = toml::from_str(DEFAULT_MADP_CONFIG).unwrap();
        assert_eq!(madp_config.madp_location, "./example");
        assert_eq!(madp_config.output, ".");
    }
    #[test]
    /**
     * 解析./example/taskid/task.config.toml文件
     */
    pub fn encode_task_config() {
        use std::{fs, path};

        use crate::config;
        let task_file_path = path::PathBuf::from("./example/taskid/task.config.toml");
        let task_config_string = fs::read_to_string(task_file_path).unwrap();
        let task_config: config::TaskConfig = match toml::from_str(&task_config_string) {
            Ok(cfg) => cfg,
            Err(err) => panic!("err: {}", err),
        };
        assert_eq!(task_config.class_component.names[0], "广告");
    }
    // #[test]
    // /**
    //  * 增加页面类型:复制./example/taskid/class/pages/my_custom_page.schema.json到
    //  * ./example/data/testapp/csii-shared/defaultTheme/admin/pages并修改config.schema.json
    //  */
    // pub fn add_page_class() {
    //     use lazy_static::lazy_static;
    //     use regex::{Captures, Regex};
    //     use std::{fs, path};

    //     const TEST_STR: &str = r#"
    //     ...some others,
    //     "enum": [
    //         "pages/tabHome.schema.json",
    //         "pages/splashScreen.schema.json"
    //       ],
    //       "options": {
    //         "enum_titles": [
    //           "主页",
    //           "闪屏"
    //         ]
    //       }
    //       some others...
    //     "#;
    //     // let str = String::from("sdfsdfsdfsdf");
    //     // let config_file_path = path::PathBuf::from(
    //     //     "./example/testapp/csii-shared/defaultTheme/admin/pages/config.schema.json",
    //     // );
    //     // let config_string = fs::read_to_string(config_file_path).unwrap();
    //     // todo 正则查找config.schema.json中的enum和enum_title,添加数据
    //     let reg_enum_id = Regex::new(r#""enum":\s*\[((\s|.)*?)\]"#).unwrap();
    //     let result = reg_enum_id.replace(TEST_STR, |caps: &Captures| {
    //         // println!("{},{},{}", &caps[0], &caps[1], &caps[2]);
    //         format!("\"enums\":[{},\n\"new_page.schema.json\n\"]", &caps[1])
    //     });
    //     println!("{}", result)
    // }

    #[test]
    fn serde_config_schema() {
        use serde_json::Value;
        use std::fs;
        let config_string = fs::read_to_string(
            "./example/testapp/data/csii-shared/defaultTheme/admin/pages/config.schema.json",
        )
        .unwrap();
        let mut parsed: Value = serde_json::from_str(&config_string).unwrap();
        // assert_eq!(
        //     parsed["properties"]["type"]["enum"][0],
        //     "pages/tabHome.schema.json"
        // );
        let enums = parsed["properties"]["type"]["enum"].as_array_mut().unwrap();
        enums.push(Value::String("newSchema.json".to_string()));
        // assert_eq!()
        // println!("{:?}", enums);
        // println!("{:?}", parsed["properties"]["type"]["enum"]);
        // enums.splice(0..0, Value::from_str("abc").unwrap());
        enums.remove(1);
        enums.insert(1, Value::String("abc.json".to_string()));
        let mut is_find = false;
        for page_schema in enums.iter() {
            println!("{}", page_schema);
            if page_schema.as_str().unwrap() == "pages/tabHome.schema.json" {
                is_find = true;
                println!("i find {}", page_schema);
            }
        }
        assert_eq!(is_find, true);
    }
    #[test]
    pub fn serde_page_list() {
        use crate::config;
        use serde_json;
        let test_json_str = r#"[{"index":"tab_user_cunzhen","name":"我的-村镇银行","src":"pages/tab_user_cunzhen.json","type":"pages/tabHome.schema.json"},{"index":"tab_deposit_cunzhen","name":"存款-村镇银行","src":"pages/tab_deposit_cunzhen.json","type":"pages/tabHome.schema.json"}]"#;
        let page_list: Vec<config::PageListItem> = serde_json::from_str(test_json_str).unwrap();
        println!("list.0.name={}", page_list[0].name);
    }
    #[test]
    pub fn get_index() {
        let test_file = "testPage.schema.json";
        let test_file_arr: Vec<&str> = test_file.split(".").collect();
        println!("{}", test_file_arr[0])
    }
}
