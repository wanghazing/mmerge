use serde::{Deserialize, Serialize};

pub const DEFAULT_MADP_CONFIG: &str = r#"
madp_location = "./example/testapp" # madp文件夹地址
output = "." # 任务输出地址
"#;

pub const DEFAULT_TASK_CONFIG: &str = r#"
# 注意：不用的配置用空字符串或空数组,不要删掉

[app_config] #应用配置
app_id = "default" # 应用id

[pre_publish_config] #定时发版
time = "2023-08-03 22:00:00"
version = "1.1.4.589"
check_unit = 30000

[class_page] #页面类型(一般不会改)
pages = []
names = []

[class_component] #组件类型
components = []
names = []
sub_schema = []

# mmerge仅支持新增页面，页面实例的修改需要考虑不同环境的差异，请手动操作
[instance_page]
pages = []
names = []
types = []
sub_schema = []

[instance_component] #组件实例
components = []
names = []
types = []
sub_schema = []

[stage] #场景
# example:
stage_ids = []
"#;
#[derive(Deserialize)]
pub struct MadpConfig {
    pub madp_location: String,
    pub output: String,
}

#[derive(Deserialize)]
pub struct TaskConfig {
    pub madp_location: String,
    pub output: String,
    pub app_config: AppConfig,
    pub pre_publish_config: PrePublishConfig,
    pub class_page: ClassPage,
    pub class_component: ClassComponent,
    pub instance_page: InstancePage,
    pub instance_component: InstanceComponent,
    pub stage: Stage,
}

#[derive(Deserialize)]
pub struct AppConfig {
    pub app_id: String,
}
#[derive(Deserialize)]
pub struct PrePublishConfig {
    pub time: String,
    pub version: String,
    pub check_unit: i64,
}

#[derive(Deserialize)]
pub struct ClassPage {
    pub pages: Vec<String>,
    pub names: Vec<String>,
}
#[derive(Deserialize)]
pub struct ClassComponent {
    pub components: Vec<String>,
    pub names: Vec<String>,
    pub sub_schema: Vec<String>,
}
#[derive(Deserialize)]
pub struct InstancePage {
    pub pages: Vec<String>,
    pub names: Vec<String>,
    pub types: Vec<String>,
    pub sub_schema: Vec<String>,
}
#[derive(Deserialize)]
pub struct InstanceComponent {
    pub components: Vec<String>,
    pub names: Vec<String>,
    pub types: Vec<String>,
    pub sub_schema: Vec<String>,
}
#[derive(Deserialize)]
pub struct Stage {
    pub stage_ids: Vec<String>,
}

#[derive(Deserialize, Serialize)]
pub struct BackUp {
    pub files: Vec<String>,
    pub origins: Vec<String>,
}

#[derive(Deserialize, Serialize)]
pub struct ComponentListItem {
    pub index: String,
    pub name: String,
    pub src: String,
    #[serde(rename = "type")]
    pub de_type: String,
}
#[derive(Deserialize, Serialize)]
pub struct PageListItem {
    pub index: String,
    pub name: String,
    pub src: String,
    #[serde(rename = "type")]
    pub de_type: String,
}
